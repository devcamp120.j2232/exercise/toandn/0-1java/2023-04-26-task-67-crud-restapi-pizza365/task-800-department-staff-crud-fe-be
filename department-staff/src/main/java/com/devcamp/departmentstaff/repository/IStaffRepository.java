package com.devcamp.departmentstaff.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.departmentstaff.model.Staff;

public interface IStaffRepository extends JpaRepository<Staff, Long> {
  Staff findById(long id);
}
