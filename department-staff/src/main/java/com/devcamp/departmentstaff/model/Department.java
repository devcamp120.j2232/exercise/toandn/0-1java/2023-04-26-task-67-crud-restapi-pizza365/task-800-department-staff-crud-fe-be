package com.devcamp.departmentstaff.model;

import javax.persistence.*;
import java.util.*;
import javax.validation.constraints.*;

import org.hibernate.validator.constraints.Range;

@Entity
@Table(name = "department")
public class Department {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotEmpty(message = "You should fill department code!")
  @Size(min = 2, message = "department code must be 2 characters!")
  @Column(name = "department_code", unique = true)
  private String departmentCode;

  @NotNull(message = "You should fill department name!")
  @Size(min = 2, message = "department name must be 2 characters!")
  @Column(name = "department_name")
  private String departmentName;

  @NotNull(message = "You should fill major!")
  @Size(min = 2, message = "major must be 2 characters!")
  @Column(name = "major")
  private String major;

  @NotNull(message = "You should fill introduction!")
  @Size(min = 2, message = "introduction must be 2 characters!")
  @Column(name = "introduction")
  private String introduction;

  @OneToMany(targetEntity = Staff.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "department_id")
  private Set<Staff> staffs;

  public Department() {
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getDepartmentCode() {
    return departmentCode;
  }

  public void setDepartmentCode(String departmentCode) {
    this.departmentCode = departmentCode;
  }

  public String getDepartmentName() {
    return departmentName;
  }

  public void setDepartmentName(String departmentName) {
    this.departmentName = departmentName;
  }

  public String getMajor() {
    return major;
  }

  public void setMajor(String major) {
    this.major = major;
  }

  public String getIntroduction() {
    return introduction;
  }

  public void setIntroduction(String introduction) {
    this.introduction = introduction;
  }

  public Set<Staff> getStaffs() {
    return staffs;
  }

  public void setStaffs(Set<Staff> staffs) {
    this.staffs = staffs;
  }

}
