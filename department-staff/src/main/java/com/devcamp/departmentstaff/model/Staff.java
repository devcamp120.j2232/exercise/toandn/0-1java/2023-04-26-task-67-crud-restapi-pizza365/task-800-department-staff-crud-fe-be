package com.devcamp.departmentstaff.model;

import javax.persistence.*;
import java.util.*;
import javax.validation.constraints.*;

import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "staff")
public class Staff {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotEmpty(message = "You should fill staff code!")
  @Size(min = 2, message = "staff code must be 2 characters!")
  @Column(name = "staff_code", unique = true)
  private String staffCode;

  @NotNull(message = "You should fill staff name!")
  @Size(min = 2, message = "staff name must be 2 characters!")
  @Column(name = "staff_name")
  private String staffName;

  @NotNull(message = "You should fill position!")
  @Size(min = 2, message = "position must be 2 characters!")
  @Column(name = "position")
  private String position;

  @NotNull(message = "You should fill sex!")
  @Size(min = 1, message = "sex must be 2 characters!")
  @Column(name = "sex")
  private String sex;

  @NotNull(message = "You should fill birthday!")
  @Size(min = 10, message = "birthday must be 10 characters!")
  @Column(name = "birthday")
  private String birthday;

  @NotNull(message = "You should fill address!")
  @Size(min = 1, message = "address must be 2 characters!")
  @Column(name = "address")
  private String address;

  @NotNull(message = "You should fill phone!")
  @Range(min = 10, message = "Must fill value greater than 10")
  @Column(name = "phone")
  private Long phone;

  @ManyToOne(fetch = FetchType.LAZY)
  @JsonIgnore
  private Department department;

  public Staff() {
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getStaffCode() {
    return staffCode;
  }

  public void setStaffCode(String staffCode) {
    this.staffCode = staffCode;
  }

  public String getStaffName() {
    return staffName;
  }

  public void setStaffName(String staffName) {
    this.staffName = staffName;
  }

  public String getPosition() {
    return position;
  }

  public void setPosition(String position) {
    this.position = position;
  }

  public String getSex() {
    return sex;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }

  public String getBirthday() {
    return birthday;
  }

  public void setBirthday(String birthday) {
    this.birthday = birthday;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Long getPhone() {
    return phone;
  }

  public void setPhone(Long phone) {
    this.phone = phone;
  }

  public Department getDepartment() {
    return department;
  }

  public void setDepartment(Department department) {
    this.department = department;
  }

}
