package com.devcamp.departmentstaff.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.departmentstaff.model.Staff;
import com.devcamp.departmentstaff.repository.IStaffRepository;

@Service
public class StaffService {
  @Autowired
  IStaffRepository pIStaffRepository;

  public ArrayList<Staff> getAllStaffs() {
    ArrayList<Staff> staffList = new ArrayList<>();
    pIStaffRepository.findAll().forEach(staffList::add);
    return staffList;
  }
}
