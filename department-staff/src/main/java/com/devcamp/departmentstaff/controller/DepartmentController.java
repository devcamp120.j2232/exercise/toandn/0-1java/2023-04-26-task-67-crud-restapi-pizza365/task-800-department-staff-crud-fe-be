package com.devcamp.departmentstaff.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.departmentstaff.model.Department;
import com.devcamp.departmentstaff.repository.IDepartmentRepository;
import com.devcamp.departmentstaff.service.DepartmentService;

@RestController
@RequestMapping("/department")
@CrossOrigin
public class DepartmentController {
  @Autowired
  IDepartmentRepository pIDepartmentRepository;
  @Autowired
  DepartmentService departmentService;

  // get all department list
  @GetMapping("/all")
  public ResponseEntity<List<Department>> getAllChapters() {
    try {
      return new ResponseEntity<>(departmentService.getAllDepartments(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }

  // get department detail by id
  @GetMapping("/detail/{id}")
  public ResponseEntity<Object> getDepartmentById(@PathVariable(name = "id", required = true) Long id) {
    Optional<Department> department = pIDepartmentRepository.findById(id);
    if (department.isPresent()) {
      return new ResponseEntity<>(department, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }

  // create new department
  @PostMapping("/create")
  public ResponseEntity<Object> createUser(@Valid @RequestBody Department pDepartment) {
    try {
      Department _department = pIDepartmentRepository.save(pDepartment);
      return new ResponseEntity<>(_department, HttpStatus.CREATED);
    } catch (Exception e) {
      System.out.println("+++++++++++++++++++++:::::" + e.getCause().getCause().getMessage());
      return ResponseEntity.unprocessableEntity()
          .body("Failed to Create specified department: " + e.getCause().getCause().getMessage());
    }
  }

  // update department
  @PutMapping("/update/{id}")
  public ResponseEntity<Object> updateDepartment(@PathVariable("id") long id,
      @Valid @RequestBody Department pDepartment) {
    try {
      Department departmentData = pIDepartmentRepository.findById(id);
      Department department = departmentData;
      department.setDepartmentCode(pDepartment.getDepartmentCode());
      department.setDepartmentName(pDepartment.getDepartmentName());
      department.setIntroduction(pDepartment.getIntroduction());
      department.setMajor(pDepartment.getMajor());
      department.setStaffs(pDepartment.getStaffs());
      try {
        return new ResponseEntity<>(pIDepartmentRepository.save(department), HttpStatus.OK);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Update specified Department: " +
                e.getCause().getCause().getMessage());
      }
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // delete chapter by id
  @DeleteMapping("/delete/{id}")
  public ResponseEntity<Department> deleteDepartment(@PathVariable("id") long id) {
    try {
      pIDepartmentRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
